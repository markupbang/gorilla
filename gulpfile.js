'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');

// paths
var DIR = {
    SRC: 'src',
    DEST: 'dist',
    MAP: 'map'
};
var SRC = { // input file
    SASS: DIR.SRC + '/sass/**/*.s+(a|c)ss', //watch
    JS: DIR.SRC + '/js/**/*.js'//watch
};
var dist = DIR.DEST; // output file

gulp.task('sass', function() {
    gulp.src(SRC.SASS)
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(sass({outputStyle: 'compact'}).on('error', sass.logError))
    .pipe(sourcemaps.write(DIR.MAP))
    .pipe(gulp.dest(dist));
});

gulp.task('js', function() {
    gulp.src(SRC.JS)
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(concat('bundle.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write(DIR.MAP))
	.pipe(gulp.dest(dist));
});

// watch
gulp.task('watch', function() {
    gulp.watch(SRC.SASS, ['sass']),
    gulp.watch(SRC.JS, ['js']);
});

gulp.task('default', ['sass','js','watch']);
