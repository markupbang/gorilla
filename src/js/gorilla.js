// lazyload
$(function() {
    $("img.lazy").lazyload();
});

// header background random
function ChangeIt(e){
    var totalCount = e;
    var num = Math.ceil( Math.random() * totalCount );
    $('.slide.header').css({'background-image': 'url(./dist/images/header_plant_' + num + '.png)'});
}
